package com.example.zzeulki.practice41;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements  WordsFragment.OnWordSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(findViewById(R.id.fragment_container) != null){
           if(savedInstanceState!=null)
               return;
        }
        WordsFragment first = new WordsFragment();
        first.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,first).commit();
    }

    public void onWordSelected(int position){
        DefinitionFragment def = (DefinitionFragment) getSupportFragmentManager().findFragmentById(R.id.definition);

        if(def!=null){
            def.updateDefinitionView(position);
        }
        else {
            DefinitionFragment newFragment = new DefinitionFragment();
            Bundle args = new Bundle();
            args.putInt(DefinitionFragment.ARG_POSITION, position);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(R.id.fragment_container, newFragment);
            transaction.addToBackStack(null);

            transaction.commit();
        }

    }
}
